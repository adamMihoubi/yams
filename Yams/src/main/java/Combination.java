import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Combination {
    private List<Integer> dices;

    Combination(List<Integer> dices) {
        this.dices = dices;
        this.dices.sort(Integer::compareTo);
    }

    Integer maximumScoreInSpecialCombination() {
        return findSpecialCombination()
                .max(Integer::compareTo)
                .get();
    }

    Integer minimumScoreInSpecialCombination() {
        return findSpecialCombination()
                .min(Integer::compareTo)
                .get();
    }

    Boolean isThereASpecialCombination() {
        return findSpecialCombination().count() > 1;
    }

    Integer maximumScore() {
        return mapToScores()
                .stream()
                .max(Integer::compareTo)
                .get();
    }

    Boolean isDicesFollowingEachOther() {
        Iterator<Integer> iterator = dices.iterator();
        Integer pastValue = iterator.next();
        while (iterator.hasNext()) {
            Integer current = iterator.next();
            if (current != pastValue + 1) return false;
            pastValue = current;
        }
        return true;
    }

    private Collection<Integer> mapToScores() {
        return dices.stream()
                .collect(Collectors.toMap(diceFaceValue -> diceFaceValue, iteration -> 1, Integer::sum))
                .values();
    }

    private Stream<Integer> findSpecialCombination() {
        return mapToScores().stream().filter(size -> size > 1);
    }
}
