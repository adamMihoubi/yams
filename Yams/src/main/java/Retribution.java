import java.util.HashMap;
import java.util.Map;

public enum Retribution {
    INSTANCE;

    private final Map<Integer, Integer> pointsBySize = new HashMap<>() {
        {
            put(1, 1);
            put(2, 3);
            put(3, 5);
            put(4, 10);
            put(5, 20);
        }
    };

    public Integer getPoints(Integer size) {
        return pointsBySize.get(size);
    }

    public Integer getPointsForSpecialCombinations(Integer maxSize, Integer minSize) {
        return maxSize.equals(minSize) ? 4 : 8;
    }

    public Integer getPointsWhenEachDiceIsFollowingTheOther() {
        return 7;
    }
}
