import java.util.List;

public class Yams {

    private Combination combination;

    public Yams(List<Integer> dices) {
        combination = new Combination(dices);
    }

    public Integer points() {
        if (combination.isDicesFollowingEachOther())
            return Retribution.INSTANCE.getPointsWhenEachDiceIsFollowingTheOther();
        return combination.isThereASpecialCombination() ? computePointsBySpecialScore() : computePointsByScore();
    }

    private Integer computePointsBySpecialScore() {
        return Retribution.INSTANCE.getPointsForSpecialCombinations
                (
                        combination.maximumScoreInSpecialCombination(),
                        combination.minimumScoreInSpecialCombination()
                );
    }

    private Integer computePointsByScore() {
        return Retribution.INSTANCE.getPoints(combination.maximumScore());
    }


}
