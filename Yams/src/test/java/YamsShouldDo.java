import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class YamsShouldDo {

    private Yams yams;

    @Test
    public void oneWhenNoCombinationFound() {
        yams = new Yams(Arrays.asList(1, 2, 6, 4, 5));
        Integer points = yams.points();
        assertEquals(Integer.valueOf(1), points);
    }

    @Test
    public void threeWhenThereIsAPair() {
        yams = new Yams(Arrays.asList(1, 1, 6, 2, 4));
        Integer points = yams.points();
        assertEquals(Integer.valueOf(3), points);
    }

    @Test
    public void fiveWhenThereIsThreeFaces() {
        yams = new Yams(Arrays.asList(1, 1, 1, 2, 4));
        Integer points = yams.points();
        assertEquals(Integer.valueOf(5), points);
    }

    @Test
    public void tenWhenThereIsFourFaces() {
        yams = new Yams(Arrays.asList(2, 2, 2, 2, 4));
        Integer points = yams.points();
        assertEquals(Integer.valueOf(10), points);
    }

    @Test
    public void twentyWhenThereIsFiveFaces() {
        yams = new Yams(Arrays.asList(4, 4, 4, 4, 4));
        Integer points = yams.points();
        assertEquals(Integer.valueOf(20), points);
    }

    @Test
    public void fourWhenItsADoublePair() {
        yams = new Yams(Arrays.asList(1, 1, 3, 2, 2));
        Integer points = yams.points();
        assertEquals(Integer.valueOf(4), points);
    }

    @Test
    public void eightWhenItsAFull() {
        yams = new Yams(Arrays.asList(1, 1, 3, 3, 3));
        Integer points = yams.points();
        assertEquals(Integer.valueOf(8), points);
    }

    @Test
    public void sevenIfEachDiceIsFollowingTheOther() {
        yams = new Yams(Arrays.asList(1, 3, 4, 5, 2));
        Integer points = yams.points();
        assertEquals(Integer.valueOf(7), points);
    }
}
